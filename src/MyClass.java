import java.io.*;
import java.util.Scanner;
import java.util.Arrays;

public class MyClass {
	
	public static String path;
	public static FileWriter fichero;
	
	public static void main(String[] args) {
	
		Scanner teclado = new Scanner(System.in);
        String fileName = null;
        boolean fileNotFound = true; 
        
        while (fileNotFound) {
	        System.out.println("Ingrese el nombre del fichero a procesar");
		    fileName = teclado.nextLine();  
		    
		    try {
		        File myFile = new File(fileName);
		        Scanner readFile = new Scanner(myFile);
		        boolean resultado = procesarFicheroMarcador(readFile);
		        if (resultado==true) {
		        	fileNotFound = false;
		        }
		    } catch (FileNotFoundException e) {
		        System.out.println("No se encontro el archivo\r\n");
		    } 
        }
	}
	
	public static boolean procesarFicheroMarcador(Scanner archivoMarcador) {
		
        int lineNumber=1;
        int rondas=0;
        int ronda=0;
        int lider=0;
        int ventaja=0;
        int acumuladoJugador1=0;
        int acumuladoJugador2=0;
        int[][] arrayResultadoRonda=new int[8][8];
        
        System.out.println("Se esta procesando el fichero");        
           
        // Lectura del fichero linea por linea
        while (archivoMarcador.hasNextLine()) {
        	   String linea = archivoMarcador.nextLine();
        	   if (lineNumber==1) {
        		   
        		   // Se obtiene la cantidad de rondas
        		   rondas=Integer.parseInt(linea);
        		   
        		   // Valida que la primer línea (número de rondas) es un entero menor o igual a 10000 
        		   if (rondas>10000) {
            		   System.out.println("");
            		   System.out.println("La primer línea es un entero mayor a 10000");
            		   System.out.println("");
            		   return false;
        		   }
        		   
        		   System.out.println("");
        		   System.out.println("Se contarán "+rondas+" rondas");
        		   System.out.println("");
        		   lineNumber=lineNumber+1;
        		   
        	   }else{
        	       
        		   	// Se procesa el marcador de cada ronda
        		   	ronda=lineNumber-1;
	        	   	String[] resultadoRonda = linea.split("\\s");
	        		   
	        		// Valida que tenga dos resultados por linea
	        		if (resultadoRonda.length!=2) {
	            		System.out.println("");
	            		System.out.println("El archivo no tiene dos resultados en la linea "+lineNumber);
	            		System.out.println("");
	            		return false;
	        		}
	        		   
	        	   	acumuladoJugador1=acumuladoJugador1+Integer.parseInt(resultadoRonda[0]);
   	       			acumuladoJugador2=acumuladoJugador2+Integer.parseInt(resultadoRonda[1]);
   	       			
   	       			// Se busca quien gano la ronda
   	       			if (acumuladoJugador1>acumuladoJugador2) {
   	       				lider=1;
	        	       	ventaja=acumuladoJugador1-acumuladoJugador2;
	        	    }else {
	        	    	lider=2;
	        	       	ventaja=acumuladoJugador2-acumuladoJugador1;
	        	    }
   	       			
   	       			// Se guardan los calculos en un arreglo
        	       	arrayResultadoRonda[0][lineNumber-2]= lider;
   	       			arrayResultadoRonda[1][lineNumber-2]= ventaja;
   	       			
   	       			System.out.println("Ronda "+ronda+" con estos resultados --> Jugador 1: "+resultadoRonda[0]+" acumulando "+acumuladoJugador1+" puntos. Jugador 2: "+resultadoRonda[1]+" acumulando "+acumuladoJugador2+" puntos. Gana el jugador: "+lider+" con ventaja de: "+ventaja);
   	       			lineNumber=lineNumber+1;
        	   }
        }
        
		// Valida que el numero guardado en la variable ronda en la ultima ronda sea = al total de rondas en la variable rondas
		if (ronda!=rondas) {
	 		System.out.println("");
	 		System.out.println("La cantidad de líneas con resultados no es igual a la de rondas "+ronda+" "+rondas);
	 		System.out.println("");
	 		return false;
		}
		   
        // Compara las rondas para ver cual es la mayor y determinar el ganador
        for (int i = 0; i < arrayResultadoRonda.length; i++) {
               if(arrayResultadoRonda[1][i] > ventaja) {
                   ventaja=arrayResultadoRonda[1][i];
                   lider=arrayResultadoRonda[0][i];
               }
        }
           
        System.out.println("");
        System.out.println("Gana el jugador "+lider+" con una ventaja de "+ventaja+" puntos");
        System.out.println("");
        guardarFicheroResultado(lider, ventaja);

        // Cerramos el fichero
        try{                    
              if( null != archivoMarcador ){   
            	  archivoMarcador.close(); 
              }                  
        }catch (Exception e2){ 
        	System.out.println("No se pudo cerrar el archivo\r\n");
        	return false;
        }
  	  	return true;
	}
	
	public static void guardarFicheroResultado(int jugador, int ventaja) {

        String fileName = "resultado.txt";
 
	    try
	    {
	    	fichero = new FileWriter(fileName);
	    	File f = new File(fileName);
	    	path = f.getAbsolutePath();
		    PrintWriter pw = new PrintWriter(fichero);	

	        // Guardamos los datos del jugador y la ventaja con la que gano
	        pw.println(jugador+" "+ventaja);
	        
	    } catch (Exception e) {
	        System.out.println("Falló en la creacion del archivo de resultado");
	        
	    } finally {
	       try {
	    	   // Aprovechamos el finally para asegurarnos que se cierra el fichero.
	    	   if (null != fichero) {
	       			fichero.close();
	       			System.out.println("El archivo del resultado se creo con éxito en "+path);	       			
	    	   }
	    	   
	       } catch (Exception e2) {
	    	   System.out.println("Falló en la creación del archivo del resultado");
	       }
	    }
	}
}
